package common

import (
	"errors"
	http2 "github.com/stretchr/testify/http"
	"testing"
)

func TestResponse(t *testing.T) {

	writer := &http2.TestResponseWriter{
		StatusCode: -1,
		Output:     "",
	}
	Response(writer, nil, errors.New(""))
}
